import {OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {ToastrService} from 'ngx-toastr';

import {UsuarioLocalStorag} from 'app/pages/usuario/_shared/usuario.modal';
import {PadraoComponent} from './padrao.component';

export class PadraoListComponent<T> extends PadraoComponent implements OnInit {
  vList: T[];
  LoggedUser: UsuarioLocalStorag;
  // PName = configuration.projectName.firstName + ' ' + configuration.projectName.lastName;

  constructor(protected databaseService: any, protected toastrService: ToastrService, protected router: Router) {
    super(databaseService);
  }

  ngOnInit() {
    // this.databaseService.getData('key_usuario', this.LoggedUser.uid).subscribe((vList: T[]) => {
    this.databaseService.getData().subscribe((rList: T[]) => {
      this.vList = rList;
      // console.log(this.vList);
    });
  }

  // ngOnInit() {
  //   this.dbService.getData().subscribe(data => {
  //     this.allEntidade = data;
  //   });
  // }

  onEdit(pDados: T) {
    this.databaseService.Dadoselected = pDados;
    this.onNavegar(this.urlRoute + '/edit');
  }

  onDelete($key?: string) {
    if (typeof $key === 'undefined') {
      this.databaseService.delete(this.keyToDelete).then(() => {
        this.toastrService.warning('Um item foi deletado!', this.NameProjeto);
      });
    } else if (typeof $key !== 'undefined') {
      this.keyToDelete = $key;
    }
  }

  onNavegar(pUrlRoute: string) {
    // console.log('navegou para ' + pUrlRoute);
    this.router.navigate([pUrlRoute]);
  }
}
