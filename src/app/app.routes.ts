import {Routes} from '@angular/router';

import {DashComponent} from './parts/dash/dash.component';
import {MainComponent} from './pages/_main/main.component';
import {LoginComponent} from './pages/_security/login/login.component';
import {LogoutComponent} from './pages/_security/logout/logout.component';

// Route Guards
import {LoggedInGuard} from './providers/route-guards/loggedin.guard';
import {NivelDevGuard} from './providers/route-guards/niveldev.guard';

export const routes: Routes = [
  {path: '', redirectTo: '/', pathMatch: 'full'},
  {path: 'create', loadChildren: './pages/_security/create-account/create-account.module#CreateAccountModule'},
  {path: 'forgot', loadChildren: './pages/_security/forgot-password/forgot-password.module#ForgotPasswordModule'},
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent},
  {
    path: '',
    component: DashComponent,
    canActivate: [LoggedInGuard],
    children: [
      {path: 'main', component: MainComponent},
      {path: 'usuario', loadChildren: './pages/usuario/usuario.module#UsuarioModule', canActivate: [NivelDevGuard]},
      {path: 'entidade', loadChildren: './pages/entidade/entidade.module#EntidadeModule'}
    ]
  }
];
