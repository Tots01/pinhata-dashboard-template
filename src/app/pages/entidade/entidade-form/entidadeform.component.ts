import {Component, OnInit} from '@angular/core';
import {FormGroup, Validators, FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

import {PadraoFormComponent} from 'app/system/components/padraoForm.component';

import {EntidadeService} from '../_shared/entidade.service';
import {Entidade} from '../_shared/entidade.model';

@Component({
  selector: 'app-entidadeform',
  templateUrl: './entidadeform.component.html',
  styleUrls: ['./entidadeform.component.scss']
})
export class EntidadeformComponent extends PadraoFormComponent implements OnInit {
  VFormGroup: FormGroup;
  entidade: Entidade = new Entidade();
  title: string;

  constructor(
    protected dbService: EntidadeService,
    protected tostr: ToastrService,
    protected formBuilder: FormBuilder,
    protected router: Router
  ) {
    super(dbService);

    this.VFormGroup = formBuilder.group({
      nome: ['', [Validators.required]],
      email: ['', [Validators.required]],
      origem: [''],
      whatsapp: ['', [Validators.required]],
      pagamentoAprovado: [false]
    });
  }

  ngOnInit() {
    console.log(this.dbService.Dadoselected);
    if (this.dbService.Dadoselected) {
      this.EstaAlterando = true;
      this.VFormGroup.setValue({
        nome: this.dbService.Dadoselected.nome || '',
        email: this.dbService.Dadoselected.email || '',
        origem: this.dbService.Dadoselected.origem || '',
        whatsapp: this.dbService.Dadoselected.whatsapp || '',
        pagamentoAprovado: this.dbService.Dadoselected.pagamentoAprovado || false
      });
      // console.log(this.VFormGroup.value);
    }
  }

  onFormSubmit() {
    const valorform = this.VFormGroup.value;
    if (!this.dbService.Dadoselected) {
      // valorform.key_usuario = this.UsuarioAtual.uid;

      this.dbService.create(valorform).then(() => {
        this.goBack();
        this.tostr.success(`${this.Nome_Tabela} criada com sucesso!`, this.NameProjeto);
      });
    } else if (this.dbService.Dadoselected) {
      valorform.$key = this.dbService.Dadoselected.$key;

      this.dbService.update(valorform).then(() => {
        this.goBack();
        this.tostr.success(`${this.Nome_Tabela}  editada com sucesso!`, this.NameProjeto);
      });
    }
  }

  goBack() {
    this.EstaAlterando = false;
    this.VFormGroup.reset();
    this.dbService.Dadoselected = undefined;
    this.router.navigate([this.urlRoute]);
  }
}
